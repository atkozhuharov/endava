# Get the locally stored credentials
gauth_credential { 'gcloud_cred':
  path 	   => "service_account.json",
  provider => serviceaccount,
  scopes   => ['https://www.googleapis.com/auth/cloud-platform',
	       'https://www.googleapis.com/auth/compute']
}

# Create the cluster
gcontainer_cluster { 'endava-poc':
  ensure             => present,
  initial_node_count => 1,
  master_auth        => {
    username => 'cluster_admin',
    password => 'n4yv9FeCET2BHxCTwzzq6ysb',
  },
  node_config        => {
    machine_type => 'n1-standard-1',
    disk_size_gb => 50,
    preemptible => true,
    oauth_scopes => [
  		"https://www.googleapis.com/auth/compute",
		"https://www.googleapis.com/auth/devstorage.read_only",
    ]
  },
  zone               => 'us-central1-a',
  project            => 'positive-apex-225210',
  credential	     => 'gcloud_cred'
}

# Get the kubeconfig so that we can deploy our app
gcontainer_kube_config { './.kubeconfig':
  ensure     => present,
  context    => "gke-endava",
  cluster    => "endava-poc",
  zone       => 'us-central1-a',
  project    => 'positive-apex-225210',
  credential => 'gcloud_cred',
}
