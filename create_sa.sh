gcloud iam service-accounts create endava-poc --description "Endava PoC" --display-name "Endava PoC"
gcloud projects add-iam-policy-binding positive-apex-225210 --member serviceAccount:endava-poc@positive-apex-225210.iam.gserviceaccount.com --role roles/editor
gcloud iam service-accounts keys create service_account.json --iam-account endava-poc@positive-apex-225210.iam.gserviceaccount.com
