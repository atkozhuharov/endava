# Initial requirements
1. Set up a GCP account and project
2. Enable the GKE API and the Cloud SQL Admin API from the GCP API library
3. Puppet, gcloud and kubectl installed as CLI tools
4. Authenticated gcloud with the GCP account from step 1: ```gcloud auth login```

# Steps
The entire process can be called from main.sh, but the steps are the following:
1. Create a service account to be used for the Kubernetes cluster and DB creation
2. Create the GCP Managed Postgres DB with puppet
3. Create the GCP Managed Kubernetes cluster with puppet
4. Apply the prepared Kubernetes manifests (deployment, service, horizontal pod autoscaler and ingress)
5. Start the service monitoring shell script
