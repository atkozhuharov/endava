gauth_credential { 'gcloud_cred':
  path     => 'service_account.json',
  provider => serviceaccount,
  scopes   => [
    'https://www.googleapis.com/auth/sqlservice.admin',
  ],
}

gsql_instance { "endava-db":
  ensure           => present,
  backend_type     => 'SECOND_GEN',
  instance_type    => 'CLOUD_SQL_INSTANCE',
  database_version => 'POSTGRES_9_6',
  settings         => {
    tier             => 'db-g1-small',
    ip_configuration => {
      authorized_networks => [
	{
	  name => "home",
	  value => '46.10.62.87/32'
	},
      ],
    },
  },
  region           => 'us-central1',
  project          => 'positive-apex-225210',
  credential       => 'gcloud_cred',
}

gsql_database { 'endava':
  ensure     => present,
  charset    => 'utf8',
  instance   => "endava-db",
  project    => 'positive-apex-225210',
  credential => 'gcloud_cred',
}

gsql_user { 'endava_admin':
  ensure     => present,
  password   => 'kf9VTX2PHczZRgrDmPRuM3Dd',
  host       => '*',
  instance   => "endava-db",
  project    => 'positive-apex-225210',
  credential => 'gcloud_cred',
}
