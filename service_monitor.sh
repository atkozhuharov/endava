INGRESS_IP=$(kubectl --kubeconfig=.kubeconfig get ing | grep endava-poc | awk '{print $3}')
while true; do
	curl -fs $INGRESS_IP > /dev/null;
	if [ $? -eq 0 ]; then 
		echo "Service is ALIVE";
	else
		echo "Service has failed - check $INGRESS_IP"
	fi
	sleep 5;
done
