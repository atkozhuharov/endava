# Launch all steps

echo "Step 1. Create GCP Service account"
./create_sa.sh

echo "Step 2. Create GCP Managed Postgres Database"
puppet apply gcloud_sql.pp

echo "Step 3. Create GCP Kubernetes cluster"
puppet apply gcloud_k8s_cluster.pp

echo "Step 4. Deploy NGINX web server"
kubectl --kubeconfig=.kubeconfig apply -f nginx-deployment.yml

echo "Step 5. Expose NGINX web server via Kubernetes Ingress + GCP LoadBalancer"
kubectl --kubeconfig=.kubeconfig apply -f ingress.yml

echo "Step 6. Initializing service monitoring shell script"
./service_monitor.sh
